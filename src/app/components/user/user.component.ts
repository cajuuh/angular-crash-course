import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name : string;
  age : number;
  email: string;
  address: Address;
  hobbies: string[];
  posts: Post[];
  isEdit: boolean = false;



  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    console.log('OnInit ran...')
    this.name = 'Pedro';
    this.age = 27;
    this.email = "pedro.alcantara@email.com"
    this.address = {
      street: 'emidio lucas da silva',
      number: 419,
      city: 'campina grande',
      state: 'paraiba'
    }
    this.hobbies = ['write code', 'watch movies', 'listen to music']

    this.dataService.getPosts().subscribe((posts) => {
      this.posts = posts;
    });
  }

  onClick(){
    this.name = 'Paulo';
  }

  addHobbie(hobbie){
    console.log(hobbie);
    this.hobbies.unshift(hobbie);
    return false;
  }

  deleteHobbie(hobbie){
    for(let i = 0; i < this.hobbies.length; i++){
      if(this.hobbies[i] == hobbie){
        this.hobbies.splice(i, 1);
      }
    }
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
  }

}

interface Address{
    street: string,
    number: number,
    city: string,
    state: string
}

interface Post{
  id: number,
  title: string,
  body: string,
  userId: number
}
